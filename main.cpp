#define DEBUG

#include "kmeans.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>


int main(int argc, char** argv){

    if ( argc != 4 )
    {
        std::cout << "\t Arg 1: Image path \n";
        std::cout << "\t Arg 2: Depth Limit \n";
        std::cout << "\t Arg 3: L value between (1, 100) (-1 if for conserve original) \n";
        return -1;
    }

    cv::Mat image;
    image = cv::imread( argv[1], 1 );

    if ( !image.data )
    {
        std::cout << "No image data \n";
        return -1;
    }

    int depth_limit = std::stoi(std::string(argv[2]));
    if(depth_limit < 4)
        depth_limit = 4;

    float Ls = std::stof(std::string(argv[3]));
    if(Ls < 0)
        Ls = -1;
    if(Ls > 100)
        Ls = 100;

    cv::namedWindow("Display Image", cv::WINDOW_AUTOSIZE );
    cv::imshow("Display Image", image);

    cv::Mat imfloat;
    image.convertTo(imfloat, CV_32FC3);
    imfloat *= 1.0/255.0;

    cv::Mat lab;
    cv::cvtColor(imfloat, lab, CV_BGR2Lab);

    KMeansCascade k;
    k.setImage(lab);
    k.makePropagation(50);
    cv::Mat clustered;
    cv::Mat outrgb;
    int d = k.getTreeDepth();
    cv::namedWindow("Clusters", cv::WINDOW_AUTOSIZE);
    std::cout << "\nDepth of tree: "<<d<< std::endl;
    
    for(int i = 0; i <= d; i++){
        std::cout << "Depth " <<i << std::endl;
        k.makeClusteredImg(clustered, i, Ls);
        cv::cvtColor(clustered, outrgb, CV_Lab2BGR);
        cv::imshow("Clusters", outrgb);
        cv::waitKey(0);
    }

    k.release();

    return 0;
}