#ifndef RANDOM_H
#define RANDOM_H

#include <random>


class MRandomFloats{
private: 
    static std::random_device rd;
    static std::mt19937 gen;
    std::uniform_real_distribution<> dist;
public:
    explicit MRandomFloats(float a, float b):
        dist(a, b)
    {

    }
    
    float getFloat(){
        return dist(MRandomFloats::gen);
    } 
};

std::random_device MRandomFloats::rd;
std::mt19937 MRandomFloats::gen(MRandomFloats::rd());

#endif