#ifndef OPGEN_H
#define OPGEN_H

//Generic Bifurcation Operation
//Makes stuff with data and divides in two parts
template <class T>
class BifGenOp{
protected: 
    T* data;
public:
    BifGenOp(){
        data = nullptr;
    }

    BifGenOp(T* data){
        this->data = data;
    }

    void setData(T* data){
        this->data = data;
    }

    T* getData(){
        return data;
    }

    virtual void op(){

    }

    virtual void reset(){
        
    }

    virtual bool check(){
        return false;
    }

    virtual T* first(){

    }

    virtual T* second(){

    }
};

#endif