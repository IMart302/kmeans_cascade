#ifndef KMEANS_H
#define KMEANS_H


#include "bifgenop.h"
#include "random.h"
#include "bnode.h"
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

class ImClusterBundle{
private:
    //these are the point corresponding to one cluster in the image
    std::vector<std::pair<int, int> > points;

    //lab color center, only a b
    std::pair<double, double> center; // center = {a, b}
    cv::Vec3f cov; // cov = {a^2, b^2, ab}
public:
    ImClusterBundle(){
        center.first = 0;
        center.second = 0;
        cov[0] = cov[1] = 1;
        cov[2] = 0;
    }

    void setPoints(std::vector<std::pair<int, int> > points){
        this->points = points;
    }

    std::vector<std::pair<int, int> > getPoints(){
        return this->points;
    }

    int size(){
        return points.size();
    }

    int clearPoints(){
        points.clear();
    }

    //this actually is the mean
    void makeCenter(cv::Mat* imgptr){
        if(points.empty())
            return;

        double a = 0;
        double b = 0;
        cv::Vec3f lab;
        for(int i = 0; i < points.size(); i++){
            lab = imgptr->at<cv::Vec3f>(points[i].second, points[i].first);
            a += lab[1];
            b += lab[2];
        }
        center.first = a/points.size();
        center.second = b/points.size();
    }

    void makeCovariance(cv::Mat* imgptr){
        if(points.size() == 0)
            return;

        cv::Vec3f pix;
        cov[0] = cov[1] = cov[2] = 0;
        float difa, difb;
        for(int p = 0; p < points.size(); p++){
            pix = imgptr->at<cv::Vec3f>(points[p].second, points[p].first);
            difa = pix[1] - center.first;
            difb = pix[2] - center.second;

            cov[0] += difa*difa;
            cov[1] += difb*difb;
            cov[2] += difa*difb;
        }

        cov = cov*(1.0/(float(points.size()) - 1.0));
    }

    void setCenter(std::pair<double, double> c){
        this->center = c;
    }

    void randomCenter(float l1, float l2){
        MRandomFloats m(l1, l2);
        center.first = m.getFloat();
        center.second = m.getFloat();
    }

    static double euclidean(std::pair<double, double> p1, std::pair<double, double> p2){
        return sqrt((p1.first - p2.first)*(p1.first - p2.first) + (p1.second - p2.second)*(p1.second - p2.second));
    }

    double centerDist(std::pair<double, double> p){
        return sqrt((p.first - center.first)*(p.first - center.first) + (p.second - center.second)*(p.second - center.second));
    }

    bool goodCovMat(){
        double det = cov[0]*cov[1] - cov[2]*cov[2];
        return (det != 0);
    }

    double centerMahaDist(std::pair<double, double> p){
        double difa = p.first - center.first;
        double difb = p.second - center.second;
        return (1.0/(cov[0]*cov[1] - cov[2]*cov[2]))*((difb*cov[1] - difb*cov[2])*difa + (difb*cov[0] - difa*cov[2])*difb);
    }

    std::pair<double, double> getCenter(){
        return center;
    }

    std::pair<int, int> getPoint(int pindx){
        return points[pindx];
    }

    void pushPoint(std::pair<int, int> p){
        points.push_back(p);
    }

    void printPoints(){
        std::cout << points[10].second << std::endl;
    }

};

class KMeansOp : public BifGenOp<ImClusterBundle>{
private: 
    cv::Mat* labimg;
    std::vector<ImClusterBundle*> clusters;
    bool infac;
public:

    KMeansOp(){
        labimg = nullptr;
        infac = true;
    }

    void setImageRef(cv::Mat* labimg){
        this->labimg = labimg;
    }
    
    void kmeans(int k, int maxit, double epsilon){
        ImClusterBundle* prim_clus = this->data;

        
        
        if(this->labimg == nullptr){
            return;
        }

        //std::cout << *labimg << std::endl;

        if(prim_clus == nullptr){
            return;
        }
        
        if(prim_clus->size() < 2){
            return;
        }

        cv::Vec3f pix;
        //clusters.resize(k);
        for(int i = 0; i < k; i++){
            clusters.push_back(new ImClusterBundle());
        }

        std::vector<std::pair<float, float> > prev_centers;

        //int step = prim_clus->size()/k;
        //std::pair<int, int> paux;
        //for(int i = 0; i < k; i++){
        //    paux = prim_clus->getPoint(i*step);
        //    pix = labimg->at<cv::Vec3f>(paux.second, paux.first);
        //    std::cout << "Center " << i << " " << pix[1] << " " << pix[2] << std::endl;
        //    clusters[i]->setCenter(std::pair<float, float>(pix[1], pix[2]));
        //}

        //random init centers
        //for(int i = 0; i < k; i++){
        //    clusters[i]->randomCenter();
        //}

        float minab = 100000, maxab = -100000;
        std::pair<int, int> paux;
        for(int p = 0; p < prim_clus->size(); p++){
            paux = prim_clus->getPoint(p);
            pix = labimg->at<cv::Vec3f>(paux.second, paux.first);
            if(pix[1] < minab)
                minab = pix[1];
            if(pix[2] < minab)
                minab = pix[2];

            if(pix[1] > maxab)
                maxab = pix[1];
            if(pix[2] > maxab)
                maxab = pix[2];
        }

        for(int i = 0; i < k; i++){
            clusters[i]->randomCenter(minab, maxab);
        }

        for(int i = 0; i < k; i++){
            prev_centers.push_back(clusters[i]->getCenter());
        }

    
        int it = 0;
        double min;
        double act_min;
        double mce;
        int cmin;
        
        std::pair<int, int> point;
        do{
            for(int i = 0; i < clusters.size(); i++){
                clusters[i]->clearPoints();
            }
            for(int p = 0; p < prim_clus->size(); p++){
                min = 100000;
                point = prim_clus->getPoint(p);
                pix = labimg->at<cv::Vec3f>(point.second, point.first);
                for(int c = 0; c < k; c++){
                    act_min = clusters[c]->centerDist(std::pair<float, float>(pix[1], pix[2]));
                    //act_min = ImClusterBundle::euclidean(std::pair<float, float>(pix[1], pix[2]), clusters[c]->getCenter());
                    if(act_min < min){
                        min = act_min;
                        cmin = c;
                    }
                }
                clusters[cmin]->pushPoint(prim_clus->getPoint(p));
            }
            for(int i = 0; i < clusters.size(); i++){
                clusters[i]->makeCenter(labimg);
            }

            mce = 0;
            for(int i = 0; i < clusters.size(); i++){
                act_min = ImClusterBundle::euclidean(clusters[i]->getCenter(), prev_centers[i]);
                mce += (act_min*act_min);
            }

            for(int i = 0; i < clusters.size(); i++){
                prev_centers[i] = clusters[i]->getCenter();
            }
            it++;
        }
        while((it < maxit) && (mce/2.0 < epsilon));
    }


    void kmeans_mahalanobis(int maxit, double epsilon){
        //centers must exist
        ImClusterBundle* prim_clus = this->data;
        int it = 0;
        double min;
        double act_min;
        double mce;
        int cmin;
        std::vector<std::pair<float, float> > prev_centers;

        do{
            for(int i = 0; i < clusters.size(); i++){
                clusters[i]->makeCovariance(labimg);
                if(!clusters[i]->goodCovMat()){ // covariance no o¿invertible
                    return;
                }
            }

            for(int i = 0; i < clusters.size(); i++){
                prev_centers.push_back(clusters[i]->getCenter());
            }

            for(int i = 0; i < clusters.size(); i++){
                clusters[i]->clearPoints();
            }

            std::pair<int, int> paux;
            cv::Vec3f pix;
            for(int p = 0; p < prim_clus->size(); p++){
                min = 100000;
                paux = prim_clus->getPoint(p);
                pix = labimg->at<cv::Vec3f>(paux.second, paux.first);
                for(int c = 0; c < clusters.size(); c++){
                    act_min = clusters[c]->centerMahaDist(std::pair<float, float>(pix[1], pix[2]));
                    if(act_min < min){
                        min = act_min;
                        cmin = c;
                    }
                }
                clusters[cmin]->pushPoint(paux);
            }

            for(int i = 0; i < clusters.size(); i++){
                clusters[i]->makeCenter(labimg);
            }

            mce = 0;
            for(int i = 0; i < clusters.size(); i++){
                act_min = ImClusterBundle::euclidean(clusters[i]->getCenter(), prev_centers[i]);
                mce += (act_min*act_min);
            }

            it++;
        }
        while((it < maxit) && (mce/2.0 < epsilon));

    }

    void releaseClusters(){
        for(int i  = 0; i < clusters.size(); i++){
            delete clusters[i];
        }
        clusters.clear();
    }

    //be sure to ref the pointers previous to clear
    void clearClusters(){
        clusters.clear();
    }

    //overloaded operation
    virtual void op(){
#ifdef DEBUG
        std::cout << "Executing kmeans ..." << std::endl;
        std::cout << "Cluster father size: " << this->data->size() << std::endl;
#endif
        kmeans(2, 1000, 1.0e-6);
        infac = false;
        if(clusters.size() == 2){
#ifdef DEBUG
            std::cout << "Kmeans finished \n";
            std::cout << "First cluster size:  " << clusters[0]->size() << " ; Second cluster size: " << clusters[1]->size() << std::endl;
#endif
            //if some cluster have cero elements then do nothing
            if(clusters[0]->size() == 0 || clusters[1]->size() == 0){
                infac = true;
                return;
            }
            else{
                if(clusters[0]->size()>2 && clusters[1]->size()>2){
#ifdef DEBUG
                    std::cout << "Executing kmeans with mahalanobis..." << std::endl;                    
#endif
                    kmeans_mahalanobis(1000, 1.6e-9);
#ifdef DEBUG
                    std::cout << "Kmeans mahalanobis finished \n";
                    std::cout << "First cluster size:  " << clusters[0]->size() << " ; Second cluster size: " << clusters[1]->size() << std::endl;
#endif
                    if(clusters[0]->size() == 0 || clusters[1]->size() == 0){
                        infac = true;
                        return;
                    }
                }
            }
        }
        else{
            infac = true;
            return;
        }
    }

    virtual ImClusterBundle* first(){
        return clusters[0];
    }

    virtual ImClusterBundle* second(){
        return clusters[1];
    }

    virtual bool check(){
        return !infac;
    }

    virtual void reset(){
        //std::cout << "overloaded reset" << std::endl;
        clearClusters();
    }
};


class KMeansCascade{
private: 
    cv::Mat image;
    BNode<ImClusterBundle>* kmeans_propagation;
public: 

    KMeansCascade(){
        kmeans_propagation = new BNode<ImClusterBundle>();
    }

    void setImage(cv::Mat img){
        this->image = img;
    }

    int getTreeDepth(){
        if(kmeans_propagation != nullptr){
            return kmeans_propagation->getDepth();
        }
    }
    
    void makePropagation(int max_depth = -1){
        KMeansOp* bikmeans = new KMeansOp();
        bikmeans->setImageRef(&image);
        ImClusterBundle* primal_cluster = new ImClusterBundle();
        std::vector<std::pair<int, int> > pinit;
        for(int x = 0; x < image.cols; x++){
            for(int y = 0; y < image.rows; y++){
                pinit.push_back(std::pair<int, int>(x, y));
            }
        }

        primal_cluster->setPoints(pinit);
        primal_cluster->printPoints();
        kmeans_propagation->set(primal_cluster);
        int depth;
        if(max_depth != -1)
            depth = max_depth;
        else 
            depth = image.rows*image.cols;

        kmeans_propagation->preorder_operation(bikmeans, depth);
        delete bikmeans;
    }

    void makeClusteredImg(cv::Mat& out, int depth, float L = -1){
        if(depth < 0){
            //get leafs;
            std::vector<BNode<ImClusterBundle>* > leafs;
            kmeans_propagation->getLeafs(leafs);
            //Every leaf have a ImCLusterBundle containing the points of the cluster
            out = cv::Mat::zeros(image.rows, image.cols, CV_32FC3);
            cv::Vec3f pix;
            ImClusterBundle* actualbundle;
            std::vector<std::pair<int, int> > ptemp;
            for(int i = 0; i < leafs.size(); i++){
                actualbundle = leafs[i]->get();
                ptemp = actualbundle->getPoints();
                for(int p = 0; p < ptemp.size(); p++){
                    if(L == -1)
                        pix[0] = image.at<cv::Vec3f>(ptemp[p].second, ptemp[p].first)[0];
                    else 
                        pix[0] = L;

                    pix[1] = actualbundle->getCenter().first;
                    pix[2] = actualbundle->getCenter().second;

                    out.at<cv::Vec3f>(ptemp[p].second, ptemp[p].first) = pix;
                }
            }
        }
        else{
            //get leafs at depth
            //get leafs;
            std::vector<BNode<ImClusterBundle>* > leafs;
            kmeans_propagation->nodesAtDepth(depth, leafs, true);
            //Every leaf have a ImCLusterBundle containing the points of the cluster
            out = cv::Mat::zeros(image.rows, image.cols, CV_32FC3);
            cv::Vec3f pix;
            ImClusterBundle* actualbundle;
            std::vector<std::pair<int, int> > ptemp;
            for(int i = 0; i < leafs.size(); i++){
                actualbundle = leafs[i]->get();
                ptemp = actualbundle->getPoints();
                for(int p = 0; p < ptemp.size(); p++){
                    if(L == -1)
                        pix[0] = image.at<cv::Vec3f>(ptemp[p].second, ptemp[p].first)[0];
                    else 
                        pix[0] = L;

                    pix[1] = actualbundle->getCenter().first;
                    pix[2] = actualbundle->getCenter().second;

                    out.at<cv::Vec3f>(ptemp[p].second, ptemp[p].first) = pix;
                }
            }
        }
    }

    void release(){
        if(kmeans_propagation != nullptr){
            kmeans_propagation->release();
        }
    }
};

#endif